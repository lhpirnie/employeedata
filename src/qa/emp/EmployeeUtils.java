package qa.emp;

public class EmployeeUtils {

    private EmployeeDAO dao;

    public EmployeeUtils(EmployeeDAO dao) {
        this.dao = dao;
    }

    public Employee getHighestPaid() {
        return dao.getAllEmployees()[1];
    }

    public Employee getLowestPaid() {
        return null;
    }

    public double getAverageSalary() {
    	double sum = 0.0;
    	Employee[] emps = dao.getAllEmployees();
    	for (Employee e : emps) {
    		sum += e.getSalary();
    	}
        return sum / emps.length;
    }

}
