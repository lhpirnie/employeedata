package qa.emp;


public class EmployeeApplication {
    public static void main(String[] args) {
    	if (args.length==0) {
    		System.out.println("Need to specify host IP as command-line parameter");
    		System.exit(0);
    	}
        EmployeeDAO dao = new EmployeeDAOImpl(args[0]); // host via command line parameter
        dao.login("root", "password");
        EmployeeUtils utils = new EmployeeUtils(dao);
       
        System.out.println("Average salary: �" + utils.getAverageSalary());
    }
}
