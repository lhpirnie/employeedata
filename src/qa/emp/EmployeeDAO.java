package qa.emp;

public interface EmployeeDAO {

	void login(String username, String password);

	Employee[] getAllEmployees();

	void updateEmployees(Employee[] employees);

	void insertEmployee(Employee employee);

}