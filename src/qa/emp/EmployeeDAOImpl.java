package qa.emp;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class EmployeeDAOImpl implements EmployeeDAO {
    private String host;
    private String username;
    private String password;

    public EmployeeDAOImpl(String host) {
        this.host = host;
    }

	public void login(String username, String password) {
        this.username = username;
        this.password = password;
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
	public Employee[] getAllEmployees() {
        List<Employee> employees = new ArrayList<>();
        try {
            Connection con = getConnection();
            Statement stmt = con.createStatement();
            ResultSet res = stmt.executeQuery("SELECT * FROM employees");
            while (res.next()) {
                int id = res.getInt(1);
                String firstname = res.getString(2);
                String lastname = res.getString(3);
                int age = res.getInt(4);
                double salary = res.getDouble(5);
                String department = res.getString(6);
                Employee emp = new Employee(id, firstname, lastname, age, salary, department);
                employees.add(emp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return (Employee[]) employees.toArray(new Employee[0]);
    }

    private Connection getConnection() throws SQLException {
    	System.out.println("Connecting to: " + "jdbc:mysql://" + host +":3306/employeedb");
        return DriverManager.getConnection("jdbc:mysql://" + host +":3306/employeedb", username, password);
    }

    @Override
	public void updateEmployees(Employee[] employees) {
        // don't!
    }

    @Override
	public void insertEmployee(Employee employee) {
        try {
            Connection con = getConnection();
            System.out.println(con);
            PreparedStatement stmt = con.prepareStatement("INSERT INTO employees VALUES (?,?,?,?,?,?)");
            stmt.setInt(1, employee.getId());
            stmt.setString(2, employee.getFirstname());
            stmt.setString(3, employee.getLastname());
            stmt.setInt(4, employee.getAge());
            stmt.setDouble(5, employee.getSalary());
            stmt.setString(6, employee.getDepartment());
            stmt.executeUpdate();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

}
