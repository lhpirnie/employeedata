package qa.emp;

public class Employee {

    private int id;
    private int age;
    private String firstname;
    private String lastname;
    private double salary;
    private String department;

    public Employee(int id, String firstname, String lastname, int age, double salary, String department) {
        this.id = id;
        this.age = age;
        this.firstname = firstname;
        this.lastname = lastname;
        this.salary = salary;
        this.department = department;
    }

    public void increaseSalary(double percent) {
        salary += salary * (percent / 100.0);
    }

    public String getFullname() {
        return firstname + " " + lastname;
    }

    public void incAge() {
        age++;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public int getId() {
        return id;
    }

    public String getDepartment() {
        return department;
    }


}

